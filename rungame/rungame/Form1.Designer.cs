﻿namespace rungame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.character = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lava1 = new System.Windows.Forms.PictureBox();
            this.lava2 = new System.Windows.Forms.PictureBox();
            this.lava3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lava4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lava5 = new System.Windows.Forms.PictureBox();
            this.lava6 = new System.Windows.Forms.PictureBox();
            this.treasure = new System.Windows.Forms.PictureBox();
            this.heart1 = new System.Windows.Forms.PictureBox();
            this.heart2 = new System.Windows.Forms.PictureBox();
            this.heart3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.character)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heart3)).BeginInit();
            this.SuspendLayout();
            // 
            // character
            // 
            this.character.BackColor = System.Drawing.Color.Transparent;
            this.character.Image = ((System.Drawing.Image)(resources.GetObject("character.Image")));
            this.character.Location = new System.Drawing.Point(77, 155);
            this.character.Name = "character";
            this.character.Size = new System.Drawing.Size(53, 97);
            this.character.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.character.TabIndex = 0;
            this.character.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(235, 135);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 126);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(769, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "current HP:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lava1
            // 
            this.lava1.Image = ((System.Drawing.Image)(resources.GetObject("lava1.Image")));
            this.lava1.Location = new System.Drawing.Point(199, 3);
            this.lava1.Name = "lava1";
            this.lava1.Size = new System.Drawing.Size(100, 126);
            this.lava1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lava1.TabIndex = 3;
            this.lava1.TabStop = false;
            // 
            // lava2
            // 
            this.lava2.Image = ((System.Drawing.Image)(resources.GetObject("lava2.Image")));
            this.lava2.Location = new System.Drawing.Point(199, 267);
            this.lava2.Name = "lava2";
            this.lava2.Size = new System.Drawing.Size(100, 185);
            this.lava2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lava2.TabIndex = 4;
            this.lava2.TabStop = false;
            // 
            // lava3
            // 
            this.lava3.Image = ((System.Drawing.Image)(resources.GetObject("lava3.Image")));
            this.lava3.Location = new System.Drawing.Point(413, 3);
            this.lava3.Name = "lava3";
            this.lava3.Size = new System.Drawing.Size(100, 196);
            this.lava3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lava3.TabIndex = 5;
            this.lava3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(457, 205);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 126);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // lava4
            // 
            this.lava4.Image = ((System.Drawing.Image)(resources.GetObject("lava4.Image")));
            this.lava4.Location = new System.Drawing.Point(413, 337);
            this.lava4.Name = "lava4";
            this.lava4.Size = new System.Drawing.Size(100, 115);
            this.lava4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lava4.TabIndex = 7;
            this.lava4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(709, 99);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 126);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // lava5
            // 
            this.lava5.Image = ((System.Drawing.Image)(resources.GetObject("lava5.Image")));
            this.lava5.Location = new System.Drawing.Point(663, 3);
            this.lava5.Name = "lava5";
            this.lava5.Size = new System.Drawing.Size(100, 90);
            this.lava5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lava5.TabIndex = 9;
            this.lava5.TabStop = false;
            // 
            // lava6
            // 
            this.lava6.Image = ((System.Drawing.Image)(resources.GetObject("lava6.Image")));
            this.lava6.Location = new System.Drawing.Point(663, 231);
            this.lava6.Name = "lava6";
            this.lava6.Size = new System.Drawing.Size(100, 221);
            this.lava6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lava6.TabIndex = 10;
            this.lava6.TabStop = false;
            // 
            // treasure
            // 
            this.treasure.BackColor = System.Drawing.Color.Transparent;
            this.treasure.Image = ((System.Drawing.Image)(resources.GetObject("treasure.Image")));
            this.treasure.Location = new System.Drawing.Point(877, 149);
            this.treasure.Name = "treasure";
            this.treasure.Size = new System.Drawing.Size(75, 76);
            this.treasure.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.treasure.TabIndex = 11;
            this.treasure.TabStop = false;
            // 
            // heart1
            // 
            this.heart1.Image = ((System.Drawing.Image)(resources.GetObject("heart1.Image")));
            this.heart1.Location = new System.Drawing.Point(836, 9);
            this.heart1.Name = "heart1";
            this.heart1.Size = new System.Drawing.Size(23, 20);
            this.heart1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heart1.TabIndex = 12;
            this.heart1.TabStop = false;
            // 
            // heart2
            // 
            this.heart2.Image = ((System.Drawing.Image)(resources.GetObject("heart2.Image")));
            this.heart2.Location = new System.Drawing.Point(865, 9);
            this.heart2.Name = "heart2";
            this.heart2.Size = new System.Drawing.Size(23, 20);
            this.heart2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heart2.TabIndex = 13;
            this.heart2.TabStop = false;
            // 
            // heart3
            // 
            this.heart3.Image = ((System.Drawing.Image)(resources.GetObject("heart3.Image")));
            this.heart3.Location = new System.Drawing.Point(894, 9);
            this.heart3.Name = "heart3";
            this.heart3.Size = new System.Drawing.Size(23, 20);
            this.heart3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heart3.TabIndex = 14;
            this.heart3.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(964, 450);
            this.Controls.Add(this.heart3);
            this.Controls.Add(this.heart2);
            this.Controls.Add(this.heart1);
            this.Controls.Add(this.treasure);
            this.Controls.Add(this.lava6);
            this.Controls.Add(this.lava5);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lava4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lava3);
            this.Controls.Add(this.lava2);
            this.Controls.Add(this.lava1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.character);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.character)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lava6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heart3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox character;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox lava1;
        private System.Windows.Forms.PictureBox lava2;
        private System.Windows.Forms.PictureBox lava3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox lava4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox lava5;
        private System.Windows.Forms.PictureBox lava6;
        private System.Windows.Forms.PictureBox treasure;
        private System.Windows.Forms.PictureBox heart1;
        private System.Windows.Forms.PictureBox heart2;
        private System.Windows.Forms.PictureBox heart3;
    }
}

