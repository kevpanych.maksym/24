﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using static System.Net.Mime.MediaTypeNames;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.CompilerServices;

namespace rungame
{
    public partial class Form1 : Form
    {
        public double timeSpent = 0;
        public int currentIndex1 = 0;
        public int currentIndex2 = 0;
        public int currentIndex3 = 0;
        public int characterHP = 3;
        bool spikes;
        public string[] arr = { "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (1).png",
            "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (2).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (3).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (4).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (5).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (6).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (7).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (8).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (9).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (10).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (11).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (12).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (13).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (14).png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder1\\Run (15).png"};
        public string[] spikeArr = { "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder2\\long_wood_spike_01.png",
        "C:\\Users\\maxke\\OneDrive\\Робочий стіл\\task\\rungame\\rungame\\NewFolder2\\long_wood_spike_05.png"};
        Thread t;
        public Form1()
        {

            InitializeComponent();
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);

            character.Image = System.Drawing.Image.FromFile(arr[currentIndex1]);
            timer1.Interval = 50;
            pictureBox1.Image= System.Drawing.Image.FromFile(spikeArr[currentIndex2]);
            pictureBox2.Image = System.Drawing.Image.FromFile(spikeArr[currentIndex2]);
            pictureBox3.Image = System.Drawing.Image.FromFile(spikeArr[currentIndex2]);

        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        //public Rectangle characterRect()
        //{
        //    int x = character.Left;
        //    int y=character.Top;
        //    int width = character.Width;
        //    int height = character.Height;
        //    return Rectangle(x + 10, y + 100,10,10) ;
        //}
        
        

        

        public void timer1_Tick(object sender, EventArgs e)
        {
            currentIndex1++;
            currentIndex3++;
            if (currentIndex1 == arr.Length)
            {
                currentIndex1 = 0;
            }
            character.Image = System.Drawing.Image.FromFile(arr[currentIndex1]);
            timeSpent += 0.5;
            if (currentIndex3 == 30)
            {
                currentIndex2++;
                if (currentIndex2 == 1)
                {
                    spikes = true;
                }
                if (currentIndex2 != 1)
                {
                    spikes=false;
                }
                if (currentIndex2 == spikeArr.Length)
                {
                    currentIndex2 = 0;
                }
                pictureBox1.Image = System.Drawing.Image.FromFile(spikeArr[currentIndex2]);
                pictureBox2.Image = System.Drawing.Image.FromFile(spikeArr[currentIndex2]);
                pictureBox3.Image = System.Drawing.Image.FromFile(spikeArr[currentIndex2]);
                currentIndex3 = 0;
            }
        }
       
        public void isCharacterAlive()
        {
            Rectangle spike1 = pictureBox1.Bounds;
            Rectangle spike2 = pictureBox2.Bounds;
            Rectangle spike3 = pictureBox3.Bounds;
            Rectangle characterRect = character.Bounds;
            
            if (characterRect.IntersectsWith(spike1)|| characterRect.IntersectsWith(spike2)|| characterRect.IntersectsWith(spike3))
            {
                if (spikes == true)
                {
                    characterHP--;
                    
                }
                
                
            }
            switch (characterHP)
            {
                case 2:heart3.Visible= false; break;
                case 1: heart2.Visible = false; break;
                case 0: heart1.Visible = false; break;
                default:break;
            }
            if (characterHP == 0)
            {
                this.Close();
                t = new Thread(openEnd);
                t.Start();
            }

        }
        public void openEnd(object sender)
        {
            System.Windows.Forms.Application.Run(new Form2());
        }
        public void openVictory(object sender)
        {
            System.Windows.Forms.Application.Run(new Form3());
        }
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            
                
                if (e.KeyCode == Keys.W)
                {
                    character.Top -= 10; // adjust the value to change the speed of movement
                }
                if (e.KeyCode == Keys.S)
                {
                    character.Top += 10; // adjust the value to change the speed of movement
                }


                if (e.KeyCode == Keys.A)
                {
                    character.Left -= 10; // adjust the value to change the speed of movement
                }


                if (e.KeyCode == Keys.D)
                {
                    character.Left += 10; // adjust the value to change the speed of movement
                }
            isCharacterAlive();
            characterIntersectsWithLava();
            characterIntersectsWithTreasure();




        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        public void characterIntersectsWithLava()
        {
            Rectangle lavaRect1 = lava1.Bounds;
            Rectangle lavaRect2 = lava2.Bounds;
            Rectangle lavaRect3 = lava3.Bounds;
            Rectangle lavaRect4 = lava4.Bounds;
            Rectangle lavaRect5 = lava5.Bounds;
            Rectangle lavaRect6 = lava6.Bounds;
            Rectangle characterRect = character.Bounds;
            if (characterRect.IntersectsWith(lavaRect1) || characterRect.IntersectsWith(lavaRect2)|| characterRect.IntersectsWith(lavaRect3)|| characterRect.IntersectsWith(lavaRect4)|| characterRect.IntersectsWith(lavaRect5)|| characterRect.IntersectsWith(lavaRect6))
            {
                this.Close();
                t = new Thread(openEnd);
                t.Start();
            }
        }
        public void characterIntersectsWithTreasure()
        {
            Rectangle characterRect = character.Bounds;
            Rectangle treasureRect=treasure.Bounds;
            if (characterRect.IntersectsWith(treasureRect))
            {
                this.Close();
                t = new Thread(openVictory);
                t.Start();
            }
        }
    }
}
